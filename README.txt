The Automated Python Blame System is designed to allow automatic source control blaming for specified lines from specified files in a codebase. This is intended to help organise common issues within a code base (such as TODO comments or Obsolete attributes), to better allow a team to tackle such problems.

An individual processor deriving from BaseChildProcessor is required for each type of issue to compile data on, and a processor is required for each source control system to extract the blame data for files. An Excel template file must be kept up to date with data that is being extracted so that the compiled output can be correctly generated.

The example Automate script requires a config file path as it's first command line argument, and the example config file here requires a codebase directory path before running.`

Prerequisites for running:
  - Python 3.x
  - The openpyxl module (http://openpyxl.readthedocs.io/en/stable/) which can be installed by PIP (python package manager).
