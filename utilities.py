import os

class LogWrapper:
    '''Wraps a text writer (e.g. stdout) and logs to the provided logger concurrently.'''
    
    def __init__(self, wrapped_writer, logger, level):
        self.wrapped_writer = wrapped_writer
        self.logger = logger
        self.level = level
        self.buffer = ''
        
    def write(self, message):
        self.wrapped_writer.write(message)
        if message.endswith('\n') or message.endswith(os.linesep):
            message = self.buffer + message
            self.buffer = ''
            for line in message.rstrip().splitlines():
                self.logger.log(self.level, line)
        else:
            self.buffer += message
    
    def flush(self):
        self.wrapped_writer.flush()
        if self.buffer != '':
            self.logger.log(self.level, self.buffer)
            self.buffer = ''
