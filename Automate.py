if (__name__ == '__main__'):

    import os
    import logging
    import sys
    import configparser
    
    from utilities  import LogWrapper
    from processors import DelegatingProcessor, TodoCommentProcessor, HackCommentProcessor, ObsoleteAttributeProcessor, MockBlameProcessor
    
    LOG_FILE   = 'Automation Log.log'
    LOG_FORMAT = '[%(asctime)s][%(name)-18s][%(levelname)-8s] : %(message)s'
    LOG_NAME   = 'Automation Logger'
    

    logging.basicConfig(level    = logging.DEBUG,
                        format   = LOG_FORMAT,
                        filename = LOG_FILE,
                        filemode = 'a')
    logger = logging.getLogger(LOG_NAME)
    sys.stdout = LogWrapper(sys.__stdout__, logger, logging.INFO)
    sys.stderr = LogWrapper(sys.__stderr__, logger, logging.ERROR)
    
    config = configparser.ConfigParser()
    config.read(sys.argv[1])
    excel  = config['EXCEL']
    source = config['SOURCE']
        
    processor = DelegatingProcessor(excel['Template'], excel['Output'], excel.get('Backup', None),
                                    source['Directory'], source.get('Prefix', ''), source['Filters'].split(',||,'),
                                    MockBlameProcessor, TodoCommentProcessor(), HackCommentProcessor(), ObsoleteAttributeProcessor())
    processor.Run()
    
    sys.stdout = sys.__stdout__
    sys.stderr = sys.__stderr__