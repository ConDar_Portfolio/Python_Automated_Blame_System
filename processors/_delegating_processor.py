import datetime
import re
import os

from openpyxl import load_workbook

class DelegatingProcessor:
    '''This processor delegates it's work to it's child processors and is the main processor for the automation.'''

    def __init__(self, excel_template_path, excel_output_path, excel_backup_path,
                       source_directory, source_directory_prefix, file_filter_regexes,
                       blame_processor, *child_processors):
        self.processors = child_processors
        self.files_for_blame = set()
        
        self.excel_template_path = excel_template_path
        self.excel_output_path   = excel_output_path
        self.excel_backup_path   = excel_backup_path
        
        self.source_directory        = source_directory
        self.source_directory_prefix = source_directory_prefix
        
        self.file_filter_regexes = file_filter_regexes
        self.blame_processor     = blame_processor
        
    def Run(self):
        '''Run the automation system delegating to child processors as necessary.'''
        self.Perform_Find()
        if os.path.exists(self.excel_output_path):
            self.Load_Previous_Output()
        self.Filter_Previous_Output()
        if len(self.files_for_blame) > 0:
            self.Run_Blame()
            self.Compile_Output()
        else:
            print('No changes requiring blame since last run')
        print('Completed blame automation successfully')
        
    def Perform_Find(self):
        '''Searches all files in the source directory to find any lines that match any of the file filter regexes and that require processing.'''
        print('Running file searches')
        for root, dirs, files in os.walk(self.source_directory):
            for file in files:
                if any(re.search(pattern, file) for pattern in self.file_filter_regexes):
                    file_path = os.path.join(root, file)
                    with open(file_path, 'r', errors='replace') as read_file:
                        file_key = file_path.startswith(self.source_directory_prefix) and file_path[len(self.source_directory_prefix):] or file_path
                        for line_num, line in enumerate(read_file.read().splitlines(), 1):
                            if self.Line_Requires_Processing(file_key, line_num, line):
                                self.files_for_blame.add(file_key)
    
    def Line_Requires_Processing(self, file, line_num, line):
        '''Delegates to child processes to pull the line if it requires processing.
           Returns true if one or more of the child processors have flagged the line for processing.'''
        any_find_results = False
        for processor in self.processors:
            if (processor.Line_Requires_Processing(file, line_num, line)):
                any_find_results = True
        return any_find_results
    
    def Load_Previous_Output(self):
        '''Loads the previously generated output into the child processes.'''
        print('Getting previously generated output')
        workbook = load_workbook(self.excel_output_path)
        for processor in self.processors:
            processor.Load_Previous_Output(workbook)
    
    def Filter_Previous_Output(self):
        '''Filter out the previously generated output from the found lines to reduce the number of blame calls required.'''
        print('Filtering out previous output to reduce number of blame calls')
        for processor in self.processors:
            processor.Filter_Previous_Output()
            
        files_to_remove = set()
        for file in self.files_for_blame:
            if not self.Requires_Blame_Run(file):
                files_to_remove.add(file)
        self.files_for_blame = self.files_for_blame - files_to_remove
    
    def Requires_Blame_Run(self, file):
        '''Returns true if any child process requires a blame call for the file.'''
        return any(processor.Requires_Blame_Run(file) for processor in self.processors)
    
    def Run_Blame(self):
        '''Runs the blame process for each file that needs processing and extracts the required data.'''
        self.current_revision = self.blame_processor.Get_Current_Revision(self.source_directory)
        print('Running automation up to revision {}'.format(self.current_revision))        
        for file in self.files_for_blame:
            print('Running blame for {}'.format(file))
            file_key = file
            if not file.startswith(self.source_directory_prefix):
                file = self.source_directory_prefix + file
            blame_data = self.blame_processor.Get_Blame_Data(file)
            self.Extract_Blame_Data(file_key, tuple(blame_data))
    
    def Extract_Blame_Data(self, file, blame_data):
        '''Delegates to the child processors to extract the required data from the blame output.'''
        for processor in self.processors:
            processor.Extract_Blame_Data(file, blame_data)
        
    def Compile_Output(self):
        '''Compiles the result of the processing to an excel file by delegating to child processes.'''
        print('Compiling output')
        workbook = load_workbook(self.excel_template_path)
        for processor in self.processors:
            processor.Compile_Output(workbook)
        workbook.template = False
        workbook.save(self.excel_output_path)
        if self.excel_backup_path is not None:
            workbook.save(self.excel_backup_path.format(revision = self.current_revision, date = datetime.datetime.today().strftime('%Y%m%d')))