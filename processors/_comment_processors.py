from ._base_child_processor import BaseChildProcessor
import re

class GeneralCommentProcessor(BaseChildProcessor):
    '''A child processor to automatically process comments matching a specific regex.'''
    
    class SpreadsheetData:
        '''A data class to hold the information from or to be output to the spreadsheet'''
        def __init__(self, committer, revision, date, content, ticket_num):
            self.committer = committer
            self.revision = revision
            self.date = date
            self.content = content
            self.ticket_num = ticket_num
    
    def __init__(self, worksheet_name, find_regex, committer_override_regex = None, ticket_number_regex = None, find_flags = re.IGNORECASE):
        super().__init__(find_regex, find_flags)
        self.worksheet_name           = worksheet_name
        self.committer_override_regex = committer_override_regex
        self.ticket_number_regex      = ticket_number_regex
        
    def Load_Previous_Output(self, workbook):
        '''Loads the previously output data from the provided workbook.'''
        if self.worksheet_name not in workbook:
            return
            
        worksheet = workbook[self.worksheet_name]
        for i in range(2, worksheet.max_row + 1):
            file        = worksheet.cell(i, 1).value
            line_number = worksheet.cell(i, 2).value
            committer   = worksheet.cell(i, 3).value
            revision    = worksheet.cell(i, 4).value
            date        = worksheet.cell(i, 5).value
            ticket_num  = worksheet.cell(i, 6).value
            content     = worksheet.cell(i, 7).value
            
            if file not in self.previous_data:
                self.previous_data[file] = {}
            self.previous_data[file][line_number] = self.SpreadsheetData(committer, revision, date, content, ticket_num)
     
    def Extract_Blame_Data(self, file, blame_data):
        '''After calling the base for extract blame data extract the following:
                - An optional override to the committer indicated by the blame.
                - An optional ticket number associated with the comment.
                
           Then update the new_data accordingly so that the correct data is output.'''
        super().Extract_Blame_Data(file, blame_data)
        for line_num in self.new_data[file]:
            line_data = self.new_data[file][line_num]
            
            # Get the optional committer override.
            if self.committer_override_regex is not None:
                assigned_comment_search_results = re.search(self.committer_override_regex, line_data.content)
                if assigned_comment_search_results:
                    committer = assigned_comment_search_results.group(1)
                else:
                    committer = line_data.committer
            else:
                committer = line_data.committer
            
            # Get the optional ticket number associated with the comment.
            if self.ticket_number_regex is not None:
                ticket_num_search_results = re.search(self.ticket_number_regex, line_data.content)
                if ticket_num_search_results:
                    ticket_num = ticket_num_search_results.group(1)
                else:
                    ticket_num = ''
            else:
                ticket_num = ''
            
            self.new_data[file][line_num] = self.SpreadsheetData(committer, line_data.revision, line_data.date, line_data.content, ticket_num)
    
    def Compile_Output(self, workbook):
        '''Output the new_data to the provided spreadsheet.'''
        worksheet = workbook[self.worksheet_name]
        N = 2
        for file in sorted(self.new_data):
            for line_num in sorted(self.new_data[file]):
                entry = self.new_data[file][line_num]

                file_cell = worksheet['A{}'.format(N)]
                file_cell.value = file
        
                line_number_cell = worksheet['B{}'.format(N)]
                line_number_cell.value = line_num
        
                committer_cell = worksheet['C{}'.format(N)]
                committer_cell.value = entry.committer
        
                revision_cell = worksheet['D{}'.format(N)]
                revision_cell.value = entry.revision
        
                date_cell = worksheet['E{}'.format(N)]
                date_cell.value = entry.date
        
                ticket_num_cell = worksheet['F{}'.format(N)]
                ticket_num_cell.value = entry.ticket_num
        
                content_cell = worksheet['G{}'.format(N)]
                content_cell.value = entry.content
                
                N += 1
                
class TodoCommentProcessor(GeneralCommentProcessor):
    def __init__(self, committer_override_regex = None, ticket_number_regex = None, find_flags = re.IGNORECASE):
        super().__init__('TODO Comments', '// *t+o+d+o*', committer_override_regex, ticket_number_regex, find_flags)
        
class HackCommentProcessor(GeneralCommentProcessor):
    def __init__(self, committer_override_regex = None, ticket_number_regex = None, find_flags = re.IGNORECASE):
        super().__init__('HACK Comments', '// *hack', committer_override_regex, ticket_number_regex, find_flags)