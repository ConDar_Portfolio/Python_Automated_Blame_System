from ._blame_processors import BlameData
import re

class BaseChildProcessor:
    '''Base class for child processors containing base functionalitiy that can be extended.
       Any class overrideing BaseChildProcessor must implement the following methods:
            - Load_Previous_Output(self, workbook)
            - Compile_Output(self, workbook)
            
       It is recommended that a subclass also override Extract_Blame_Data method calling the
            base implementation before modifying the result to get specific data needed for output.'''

    def __init__(self, find_regex, find_flags):
        self.find_regex    = find_regex
        self.find_flags    = find_flags
        self.find_data     = {}
        self.previous_data = {}
        self.new_data      = {}
        
    def Line_Requires_Processing(self, file, line_num, line):
        '''Returns true if the find_regex matches the current line.'''
        if (re.search(self.find_regex, line, self.find_flags)):
            if file not in self.find_data:
                self.find_data[file] = {}
            self.find_data[file][line_num] = line.lstrip()
            return True
        return False
    
    def Filter_Previous_Output(self):
        '''Filters out any lines that have not changed since the last output was generated.'''
        if len(self.previous_data) == 0:
            return
        
        files_to_pop = []
        for file in self.find_data:
            if file not in self.new_data:
                self.new_data[file] = {}
                
            lines_to_pop = []
            for line_num in self.find_data[file]:
                if self.Blame_Line_In_Previous_Data(file, line_num):
                    self.new_data[file][line_num] = self.previous_data[file][line_num]
                    lines_to_pop.append(line_num)
            
            # Remove the lines that have not changed from the find_data.
            for line_num in lines_to_pop:
                self.find_data[file].pop(line_num)
            
            # If there are no more line differences in the find_data for a file set that file for removal later.
            if len(self.find_data[file]) == 0:
                files_to_pop.append(file)
        
        # Remove the files that no longer have any lines that differ from the previous output.
        for file in files_to_pop:
            self.find_data.pop(file)

    def Blame_Line_In_Previous_Data(self, file, line_num):
        '''Checks if the previous data has a specific blame line match with the find_data.'''
        if file not in self.previous_data:
            return False
        
        if line_num not in self.previous_data[file]:
            return False
            
        if self.find_data[file][line_num] != self.previous_data[file][line_num].content:
            return False
            
        return True
                
    def Requires_Blame_Run(self, file):
        '''Blame runs are needed for files in find_data after filtering.'''
        return file in self.find_data
        
    def Extract_Blame_Data(self, file, blame_data):
        '''This base method extracts the basic blame information for found lines the provided file.'''
        if file not in self.new_data:
            self.new_data[file] = {}
        
        if file not in self.find_data:
            return
        
        for line_num in self.find_data[file]:
            content = self.find_data[file][line_num]
            
            if len(blame_data) < line_num:
                self.new_data[file][line_num] = BlameData.UnknownData(content)
            else:
                self.new_data[file][line_num] = blame_data[line_num - 1]