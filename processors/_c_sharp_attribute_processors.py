from ._base_child_processor import BaseChildProcessor
from ._blame_processors     import BlameData
import re

class CSharpAttributeProcessor(BaseChildProcessor):
    '''A child processor to automatically process C# attributes'''
    
    class SpreadsheetData:
        '''A data class to store the datat that will be output into the spreadsheet'''
        def __init__(self, committer, revision, date, content, ticket_num):
            self.committer   = committer
            self.revision    = revision
            self.date        = date
            self.content     = content
            self.ticket_num  = ticket_num
    
    # Checks if the line starts with an attribute.
    ATTRIBUTE_REGEX = r'^ *\['
    
    def __init__(self, worksheet_name, attribute_name_regex, blame_line_after_attributes = True, committer_override_regex = None, ticket_number_regex = None, find_flags = 0):
        super().__init__(r'\[ *{}(Attribute)?.*\]'.format(attribute_name_regex), find_flags)
        self.worksheet_name              = worksheet_name
        self.blame_line_after_attributes = blame_line_after_attributes
        self.committer_override_regex    = committer_override_regex
        self.ticket_number_regex         = ticket_number_regex
    
    def Load_Previous_Output(self, workbook):
        '''Loads the previously output data from the provided workbook.'''
        if self.worksheet_name not in workbook:
            return
            
        worksheet = workbook[self.worksheet_name]
        for i in range(2, worksheet.max_row + 1):
            file        = worksheet.cell(i, 1).value
            line_number = worksheet.cell(i, 2).value
            committer   = worksheet.cell(i, 3).value
            revision    = worksheet.cell(i, 4).value
            date        = worksheet.cell(i, 5).value
            ticket_num  = worksheet.cell(i, 6).value
            content     = worksheet.cell(i, 7).value
            
            if file not in self.previous_data:
                self.previous_data[file] = {}
            self.previous_data[file][line_number] = self.SpreadsheetData(committer, revision, date, content, ticket_num)
        
    def Extract_Blame_Data(self, file, blame_data):
        '''After calling the base for extract blame data optionally update the blame data to be drawn from the next non-attribute, non-whitespace line
                after the found attribute line, then extract the following data:
                - An optional override to the committer indicated by the blame.
                - An optional ticket number associated with the comment.
                
           Then update the new_data accordingly so that the correct data is output.'''
        super().Extract_Blame_Data(file, blame_data)
        
        if (self.blame_line_after_attributes):
            self.Update_Blame_Data_To_Line_After_Attributes(file, blame_data)
        
        for line_num in self.new_data[file]:
            line_data = self.new_data[file][line_num]
            
            if self.committer_override_regex is not None:
                assigned_comment_search_results = re.search(self.committer_override_regex, line_data.content)
                if assigned_comment_search_results:
                    committer = assigned_comment_search_results.group(1)
                else:
                    committer = line_data.committer
            else:
                committer = line_data.committer
            
            if self.ticket_number_regex is not None:
                ticket_num_search_results = re.search(self.ticket_number_regex, line_data.content)
                if ticket_num_search_results:
                    ticket_num = ticket_num_search_results.group(1)
                else:
                    ticket_num = ''
            else:
                ticket_num = ''
            
            self.new_data[file][line_num] = self.SpreadsheetData(committer, line_data.revision, line_data.date, line_data.content, ticket_num)
            
    def Update_Blame_Data_To_Line_After_Attributes(self, file, blame_data):
        '''For each blame line in new_data update that line to draw everything but it's content from
                the next line that isn't whitespace, empty or an attribute line'''
        for line_num in self.new_data[file]:            
            for i in range(line_num, len(blame_data)):
                if blame_data[i] is None or blame_data[i].content.isspace() or re.search(self.ATTRIBUTE_REGEX, blame_data[i].content):
                    continue
                    
                self.new_data[file][line_num] = BlameData(blame_data[i].revision, blame_data[i].committer,
                                                          blame_data[i].date, self.new_data[file][line_num].content)
                break
                
    def Compile_Output(self, workbook):
        '''Output the new_data to the provided spreadsheet.'''
        worksheet = workbook[self.worksheet_name]
        N = 2
        for file in sorted(self.new_data):
            for line_num in sorted(self.new_data[file]):
                entry = self.new_data[file][line_num]

                file_cell = worksheet['A{}'.format(N)]
                file_cell.value = file
        
                line_number_cell = worksheet['B{}'.format(N)]
                line_number_cell.value = line_num
        
                committer_cell = worksheet['C{}'.format(N)]
                committer_cell.value = entry.committer
        
                revision_cell = worksheet['D{}'.format(N)]
                revision_cell.value = entry.revision
        
                date_cell = worksheet['E{}'.format(N)]
                date_cell.value = entry.date
        
                ticket_num_cell = worksheet['F{}'.format(N)]
                ticket_num_cell.value = entry.ticket_num
        
                content_cell = worksheet['G{}'.format(N)]
                content_cell.value = entry.content
                
                N += 1
                
class ObsoleteAttributeProcessor(CSharpAttributeProcessor):
    def __init__(blame_line_after_attributes = True, committer_override_regex = None, ticket_number_regex = None, find_flags = 0):
        super().__init__('Obsolete Attributes', 'Obsolete', blame_line_after_attributes, committer_override_regex, ticket_number_regex, find_flags)
