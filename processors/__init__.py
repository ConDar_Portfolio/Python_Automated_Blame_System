from ._delegating_processor         import DelegatingProcessor
from ._comment_processors           import GeneralCommentProcessor, TodoCommentProcessor, HackCommentProcessor
from ._blame_processors             import SVNProcessor, MockBlameProcessor
from ._c_sharp_attribute_processors import CSharpAttributeProcessor, ObsoleteAttributeProcessor
