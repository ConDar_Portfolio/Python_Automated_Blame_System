import re
import subprocess
import datetime

class BlameData:
    '''A data class to hold the data extracted from a line of the blame output'''
    def __init__(self, revision, committer, date, content):
        self.revision  = revision
        self.committer = committer
        self.date      = date
        self.content   = content
        
    def UnknownData(content):
        return BlameData('Unknown', 'Unknown', 'Unknown', content)

class SVNProcessor:
    SVN_REVISION_REGEX = r'Revision: (\d+)'
    
    # This regex extracts the following groups from a line of the verbose svn blame output:
    #   \d+               -> The revision number the line was last modified in.
    #   [A-Za-z]{3,}      -> The initials of the developer who committed the last modification.
    #   \d{4}-\d{2}-\d{2} -> The date the last modification was committed.
    #   .*                -> The content of the source file line.
    SVN_BLAME_REGEX = r' *(\d+) *([A-Za-z]+) (\d{4}-\d{2}-\d{2}) [^\)]*\) *(.*)'
    
    def Get_Current_Revision(directory):
        '''Gets the current revision number of the provided directory'''
        subprocess_output = subprocess.run(['svn', 'info', directory], stdout=subprocess.PIPE, encoding='ISO-8859-1')
        search_results = re.search(SVN_REVISION_REGEX, subprocess_output.stdout)
        assert search_results, "Unable to get current revision for directory {}".format(directory)
        return search_results.group(1)
        
    def Get_Blame_Data(file):
        '''Gets the blame data from a given file'''
        subprocess_output = subprocess.run(['svn', 'blame', self.SVN_FILE_FORMAT.format(file), '-r1:{}'.format(self.current_revision), '-v'],
                                           stdout=subprocess.PIPE, encoding='ISO-8859-1')
        return (Extract_Blame_Data(line) for line in subprocess_output.stdout.splitlines())
    
    def Extract_Blame_Data(raw_blame_line):
        '''Gets the blame data for a given line'''
        search_results = re.search(self.SVN_BLAME_REGEX, raw_blame_line)
        if not search_results(blame_data):
            blame_data.append(BlameData.UnknownData(line))
        
        revision  = search_results.group(1)
        committer = search_results.group(2)
        date      = search_results.group(3)
        blame_data.append(BlameData(revision, committer, date, content))
        
class MockBlameProcessor:
    N = 0
    def Get_Current_Revision(directory):
        return hex(hash(str(directory) + str(datetime.datetime.now())))[-4:]
    
    def Get_Blame_Data(file):
        with open(file, 'r', errors='replace') as f:
            MockBlameProcessor.N += 1
            return (BlameData(MockBlameProcessor.N, '???', 'someday', line.lstrip()) for i, line in enumerate(f.read().splitlines(), 1)) 